import XCTest
import RealmSwift
@testable import NewsFeeder

class LocalDataTests: XCTestCase {
    var dataManager: DataManager!
    let previousRealmFileURL = Realm.Configuration.defaultConfiguration.fileURL!
    var realm: Realm! = nil
    
    override func setUp() {
        super.setUp()
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = "test-database"
        self.realm = try! Realm()
        dataManager = DataManager(realm: self.realm)
    }
    
    override func tearDown() {
        super.tearDown()
        realm = nil
        dataManager = nil
    }
    
    func testSavingToRR() {
        //GIVEN
        let article = Article(title: "title", description: "desc", link: "url.pl", guid: "url.pl/2", pubDate: "Jun 3 2019")
        
        //WHEN
        dataManager.saveToReadingRoom(article: article)
        
        //THEN
        XCTAssertEqual((realm.objects(Article.self).filter("guid = \"url.pl/2\"")).isEmpty, false)
        XCTAssertEqual((realm.objects(Article.self).filter("guid = \"url.pl/2\"")).count, 1)
    }
    
    func testDeletingFromRR() {
        //GIVEN
        let article = Article(title: "title", description: "desc", link: "url.pl", guid: "url.pl/2", pubDate: "Jun 3 2019")
        dataManager.saveToReadingRoom(article: article)
        
        //WHEN
        dataManager.removeFromReadingRoom(article: article)
        
        //THEN
        XCTAssertEqual((realm.objects(Article.self).filter("guid = \"url.pl/2\"")).isEmpty, true)
    }
    
    func testHandlingCategoryRenamingToNil() {
        //GIVEN
        let category = Category(name: "name", channels: [])
        _ = dataManager.addCategory(category)
        
        //WHEN&THEN
        XCTAssertThrowsError(try dataManager.handleCategoryRenaming(category, "nil")) { error in
            XCTAssertEqual(error as! Utilities.NewsFeederErrors, Utilities.NewsFeederErrors.invalidCategoryName)
        }
    }
    
    func testHandlingCategoryRenamingToNothing() {
        //GIVEN
        let category = Category(name: "name", channels: [])
        _ = dataManager.addCategory(category)
        
        //WHEN&THEN
        XCTAssertThrowsError(try dataManager.handleCategoryRenaming(category, "    ")) { error in
            XCTAssertEqual(error as! Utilities.NewsFeederErrors, Utilities.NewsFeederErrors.invalidCategoryName)
        }
    }
    
    func testHandlingCategoryRenamingWithChannel() {
        //GIVEN
        dataManager.addChannelToLocalDB(with: "url.pl")
        let channel = dataManager.getChannels()[0]
        let category = Category(name: "name", channels: [channel])
        _ = dataManager.addCategory(category)
        let newName = "  newName  "
        
        //WHEN
        try! dataManager.handleCategoryRenaming(category, newName)
        let channelToCheck = Array(realm.objects(Channel.self).filter("category = %@", category))[0]
        let catToCheck = Array(realm.objects(Category.self))[0]
        print(catToCheck)
        
        //THEN
        XCTAssertEqual(channelToCheck.category?.name, newName)
        XCTAssertEqual(catToCheck.name, newName)
        XCTAssertEqual(catToCheck.channels[0].category?.name, newName)
    }
    
    func testRemovingCategory() {
        //GIVEN
        dataManager.addChannelToLocalDB(with: "url.m1.pl")
        let channel = dataManager.getChannels()[0]
        let category = Category(name: "cat", channels: [channel])
        _ = dataManager.addCategory(category)
        
        //WHEN
        dataManager.removeCategory(category)
        let channelToCheck = Array(realm.objects(Channel.self))
        
        //THEN
        XCTAssertEqual(realm.objects(Category.self).isEmpty, true)
        XCTAssertEqual(channelToCheck[0].category, nil)
    }
    
    func testAddingCategory() {
        //GIVEN
        dataManager.addChannelToLocalDB(with: "url.m1.pl")
        let channel = dataManager.getChannels()[0]
        let category = Category(name: "cat", channels: [channel])
        
        //WHEN
        let result = dataManager.addCategory(category)
        
        //THEN
        XCTAssertEqual(result, true)
        XCTAssertEqual(realm.objects(Category.self).isEmpty, false)
    }
    
    func testAddingCategoryWithTheSameName() {
        //GIVEN
        dataManager.addChannelToLocalDB(with: "url.m1.pl")
        let channel = dataManager.getChannels()[0]
        let category1 = Category(name: "cat", channels: [channel])
        _ = dataManager.addCategory(category1)
        let category2 = Category(name: "cat", channels: [channel])
        
        //WHEN
        let result = dataManager.addCategory(category2)
        let channelToCheck = Array(realm.objects(Channel.self))
        
        //THEN
        XCTAssertEqual(result, false)
        XCTAssertEqual(realm.objects(Category.self).isEmpty, false)
        XCTAssertNil(channelToCheck[0].category)
    }
    
    func testModifingCategory() {
        //GIVEN
        let url = "url.m1.pl"
        dataManager.addChannelToLocalDB(with: url)
        let channel = dataManager.getChannels()[0]
        let category = Category(name: "cat", channels: [])
        _ = dataManager.addCategory(category)
        
        //WHEN
        let before = category.channels.count
        dataManager.modifyCategory(with: channel, category)
        let after = category.channels.count
        let result = category.channels.first!.url
        
        //THEN
        XCTAssertEqual(before, 0)
        XCTAssertEqual(after, 1)
        XCTAssertEqual(result, "url.m1.pl")
    }
    
    func testModifingChannel() {
        //GIVEN
        dataManager.addChannelToLocalDB(with: "url.m1.pl")
        let channel = dataManager.getChannels()[0]
        let category = Category(name: "cat", channels: [])
        _ = dataManager.addCategory(category)
        
        //WHEN
        let before = Array(realm.objects(Channel.self).filter("category == %@", category)).count
        dataManager.modifyCategory(with: channel, category)
        dataManager.modifyChannel(with: category, channel)
        let after = realm.objects(Channel.self).filter("category == %@", category)
        
        //THEN
        XCTAssertEqual(before, 0)
        XCTAssertEqual(after.count, 1)
    }
    
    func testModifingChannelWithNil() {
        //GIVEN
        let category = Category(name: "cat", channels: [])
        _ = dataManager.addCategory(category)
        dataManager.addChannelToLocalDB(with: "url.m1.pl", to: category)
        let channel = dataManager.getChannels(true).first!
        
        //WHEN
        let before = Array(realm.objects(Channel.self).filter("category == %@", category)).count
        dataManager.modifyChannel(channel)
        let after = realm.objects(Channel.self).filter("category == %@", category)
        
        //THEN
        XCTAssertEqual(before, 1)
        XCTAssertEqual(after.count, 0)
    }
    
    func testGetChannels() {
        //GIVEN
        let category = Category(name: "cat", channels: [])
        _ = dataManager.addCategory(category)
        dataManager.addChannelToLocalDB(with: "url.m3.pl", to: category)
        
        dataManager.addChannelToLocalDB(with: "url.m4.pl")
        dataManager.addChannelToLocalDB(with: "url.m1.pl")
        dataManager.addChannelToLocalDB(with: "url.m2.pl")
        
        //WHEN
        let result = Array(dataManager.getChannels())
        
        //THEN
        XCTAssertEqual(result.count, 3)
    }
    
    func testGetAllChannels() {
        //GIVEN
        let category = Category(name: "cat", channels: [])
        _ = dataManager.addCategory(category)
        dataManager.addChannelToLocalDB(with: "url.m3.pl", to: category)
        
        dataManager.addChannelToLocalDB(with: "url.m4.pl")
        dataManager.addChannelToLocalDB(with: "url.m1.pl")
        dataManager.addChannelToLocalDB(with: "url.m2.pl")
        
        //WHEN
        let result = Array(dataManager.getChannels(true))
        
        //THEN
        XCTAssertEqual(result.count, 4)
    }
    
    func testGetChannelsFromCategory() {
        //GIVEN
        let category = Category(name: "cat", channels: [])
        _ = dataManager.addCategory(category)
        dataManager.addChannelToLocalDB(with: "url.m3.pl", to: category)
        
        dataManager.addChannelToLocalDB(with: "url.m4.pl")
        dataManager.addChannelToLocalDB(with: "url.m1.pl")
        dataManager.addChannelToLocalDB(with: "url.m2.pl")
        
        //WHEN
        let result = Array(dataManager.getChannels(from: category))
        
        //THEN
        XCTAssertEqual(result.count, 1)
    }
}
