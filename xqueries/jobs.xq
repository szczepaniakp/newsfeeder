xquery version "3.0";

module namespace jobs="http://szczepaniak.patrycja.com/newsfeeder/jobs";
import module namespace writer="http://szczepaniak.patrycja.com/newsfeeder/writer" at "writer.xq";
declare namespace atom = "http://www.w3.org/2005/Atom";


declare updating function jobs:download-feed(
)
{
let $doc := db:open("NewsFeeder", "/channels/")

for $channel in $doc/channel
return
if (xs:integer($channel/users) eq 0)
then
()
else
jobs:parse-channel($channel/@uri)
};

declare updating function jobs:parse-channel(
$channel-id
)
{
let $channel-xml := doc($channel-id)
return (
if (string-length($channel-xml/rss/channel/string()) != 0)
then (
for $article in $channel-xml/rss/channel/item
let $article-xml :=
<article channel-id="{data($channel-id)}" datetime="{current-dateTime()}" >
<title>{$article/((node()[local-name() = ('title', 'name')])/.)[1]/string()}</title>
<link>{$article/((node()[local-name() = ('link', 'url')])/.)[1]/string()}</link>
<guid>{$article/((node()[local-name() = ('guid', 'link', 'url')])/.)[1]/string()}</guid>
<pubDate>{$article/((node()[local-name() = ('pubDate', 'date')])/.)[1]/string()}</pubDate>
<description>{$article/((node()[local-name() = ('description')])/.)[1]/string()}</description>
</article>

let $uri := ("/articles/" || replace(writer:trim($article-xml/guid), "/", "-"))
return
if(empty(db:open("NewsFeeder", $uri)))
then
db:add("NewsFeeder", $article-xml, $uri)
else
()
)
else if (string-length($channel-xml/atom:feed/string()) != 0)
then (
for $article in $channel-xml/atom:feed/atom:entry
let $article-xml :=
<article channel-id="{data($channel-id)}" datetime="{current-dateTime()}">
<title>{$article/atom:title/string()}</title>
<link>{$article/atom:link/@href/string()}</link>
<guid>{$article/atom:id/string()}</guid>
<pubDate>{$article/atom:updated/string()}</pubDate>
<description>{$article/atom:summary/string(), $article/atom:content/string()}</description>
</article>

let $uri := ("/articles/" || replace(writer:trim($article-xml/guid), "/", "-"))
return
if(empty(db:open("NewsFeeder", $uri)))
then
db:add("NewsFeeder", $article-xml, $uri)
else
()
)
else ()
)
};

declare updating function jobs:clean-db(
)
{
let $doc := db:open("NewsFeeder", "/articles/")

for $article in $doc/article
return
if (xs:dayTimeDuration(current-dateTime() - xs:dateTime($article/@datetime)) >  xs:dayTimeDuration("P7D"))
then (
let $uri := "/articles/" || replace(writer:trim($article/guid), "/", "-")
return db:delete("NewsFeeder", $uri)
)
else
()
};



