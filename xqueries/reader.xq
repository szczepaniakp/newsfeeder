xquery version "3.1";

module namespace reader = "http://szczepaniak.patrycja.com/newsfeeder/reader";

declare
%rest:path("/get-articles")
%rest:PUT("{$data}")
%rest:produces("application/json;qs=1")
%rest:consumes("application/json")
function reader:get-articles(
$data
)
{
let $doc := db:open("NewsFeeder", "/articles/")
let $xml := <json type="object">
{
for $article in $doc/article
return (if (contains($data, $article//@channel-id/string()))
then
<article type="object">
<title>{$article/title/string()}</title>
<link>{$article/link/string()}</link>
<guid>{$article/guid/string()}</guid>
<pubDate>{$article/pubDate/string()}</pubDate>
<description>{$article/description/string()}</description>
</article>
else
()
)
}
</json>

return (<rest:response>
<http:response status="200">
<http:header name="Content-Language" value="en"/>
<http:header name="Content-Type" value="application/json; charset=utf-8"/>
</http:response>
</rest:response>, json:serialize($xml))
};

