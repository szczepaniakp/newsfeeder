xquery version "3.0";
module namespace writer="http://szczepaniak.patrycja.com/newsfeeder/writer";
import module namespace jobs="http://szczepaniak.patrycja.com/newsfeeder/jobs" at "jobs.xq";
declare namespace atom = "http://www.w3.org/2005/Atom";

declare
%updating
%rest:path("/add-channel")
%rest:PUT("{$data}")
%rest:consumes("application/json")
%rest:produces("text/plain;charset=utf-8")
function writer:add-channel(
$data
)
{
let $uri := "/channels/" || replace(writer:trim($data//url/string()), "/", "-")
let $xml := writer:check-channel($data)

return if(string($xml) eq '')
then
fn:error(xs:QName("err:INVALID-URL"))
else (
if(empty(db:open("NewsFeeder", $uri)))
then (
db:add("NewsFeeder", $xml, $uri),
jobs:parse-channel(writer:trim(writer:check-last-char($data//url/string())))
)
else
writer:increment-users($uri)
)
};

declare
%updating
%rest:path("/remove-channel")
%rest:PUT("{$data}")
%rest:consumes("application/json")
function writer:remove-channel(
$data
)
{
let $uri := "/channels/" || replace(writer:trim($data//url/string()), "/", "-")
return
if(not(empty(db:open("NewsFeeder", $uri))))
then
writer:decrement-users($uri)
else
()
};

declare function writer:check-channel(
$data
)
{
let $url := writer:trim(writer:check-last-char($data//url/string()))

return
if (fn:doc-available($url))
then (
let $source := doc($url)
return
(if (string-length($source/rss/channel/string()) != 0)
then (
let $channel := doc($url)/rss/channel
let $xml :=
<channel uri="{data($url)}">
<title>{$channel/((node()[local-name() = ('title', 'name')])/.)[1]/string()}</title>
<url>{$channel/((node()[local-name() = ('link')])/.)[1]/string()}</url>
<users>1</users>
<pubDate>{$channel/((node()[local-name() = ('lastBuildDate', 'pubDate')])/.)[1]/string()}</pubDate>
<description>{$channel/node()[local-name() = ('description')]/string()}</description>
</channel>
return $xml
)

else if (string-length($source/atom:feed/string()) != 0)
then (
let $channel := doc($url)/atom:feed
let $xml :=
<channel uri="{data($url)}">
<title>{$channel/atom:title/string()}</title>
<url>{$channel/atom:link}</url>
<users>1</users>
<pubDate>{$channel/atom:updated/string()}</pubDate>
<description>{$channel/atom:subtitle/string()}</description>
</channel>
return $xml
)
else ()
)
)

else
()

};

declare function writer:check-last-char(
$url
)
{
if (substring($url, string-length($url)) = '/')
then
substring($url, 1, string-length($url)-1)
else
$url
};

declare updating function writer:decrement-users(
$uri
)
{
let $users := db:open('NewsFeeder', $uri)/channel/users
return replace value of node $users with ($users - 1)
};

declare updating function writer:increment-users(
$uri
)
{
let $users := db:open('NewsFeeder', $uri)/channel/users
return replace value of node $users with ($users + 1)
};

declare function writer:trim(
$arg as xs:string?
)  as xs:string {
replace(replace($arg,'\s+$',''),'^\s+','')
};

declare
%rest:error("err:INVALID-URL")
function writer:invalid-url-caught(
)
{
<rest:response>
<http:response status="400">
<http:header name="Content-Language" value="en"/>
<http:header name="Content-Type" value="plain/text; charset=utf-8"/>
</http:response>
</rest:response>
};



