import Foundation
import UIKit

class Utilities {
    static func showAlert(_ message: String, controller: UIViewController? = nil, completion: @escaping (UIAlertController) -> ()) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                if controller != nil {
                    controller?.navigationController?.popViewController(animated: true)
                }
            }))
            
            completion(alert)
        }
    }
    
    enum NewsFeederErrors: Error {
        case invalidURL
        case invalidResponse
        case invalidCategoryName
        case noChannelsProvided
    }
}
