import Foundation
import RealmSwift
import Realm

class DataManager: NSObject {
    
    let encoding = String.Encoding.utf8
    var feed: [Article] = []
    var realm: Realm!
    
    init(realm: Realm) {
        self.realm = realm
    }
    
    override convenience init() {
        self.init(realm: try! Realm())
    }
    
    func addChannel(from url: String, completion: @escaping (Any?) -> ()) {
        if realm.objects(Channel.self).filter("url = \"\(url)\"").count > 0 {
            completion(true)
            return
        }
        
        var request = URLRequest(url: URL(string:"http://54.37.139.78:80/add-channel")!)
        request.httpMethod = "PUT"
        request.httpBody = try? JSONSerialization.data(withJSONObject: ["url": url])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = DataManager.defaultURLSession()
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            if error != nil {
                NSLog("Server error.")
                completion(error)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse, !((200...299).contains(httpResponse.statusCode)){
                NSLog("Response error.")
                completion(NSURLErrorBadURL)
                return
            }
            
            completion(nil)
        })
        
        task.resume()
    }
    
    func addChannelToLocalDB(with url: String, to category: Category? = nil) {
        let channel = Channel(url, category)
        try! realm.write {
            realm.add(channel)
            if category != nil {
                category!.channels.append(channel)
            }
        }
    }
    
    func removeChannel(_ channel: Channel, permanent: Bool = false) {
        try! realm.write {
            if permanent {
                var request = URLRequest(url: URL(string:"http://54.37.139.78:80/remove-channel")!)
                request.httpMethod = "PUT"
                request.httpBody = try? JSONSerialization.data(withJSONObject: ["url": channel.url])
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let session = DataManager.defaultURLSession()
                
                let task = session.dataTask(with: request, completionHandler: { data, response, error in
                    if error != nil {
                        NSLog("Server error.")
                        return
                    }
                    
                    if let httpResponse = response as? HTTPURLResponse, !((200...299).contains(httpResponse.statusCode)){
                        NSLog("Response error.")
                        return
                    }
                })
                
                realm.delete(channel)
                
                task.resume()
            } else {
                let index = channel.category?.channels.index(of: channel)
                channel.category?.channels.remove(at: index!)
                channel.category = nil
            }
        }
    }
    
    func getChannels(from category: Category? = nil,_ all: Bool = false) -> [Channel] {
        if all {
            return Array(realm.objects(Channel.self).sorted(byKeyPath: "url"))
        }
        else if category == nil {
            return Array(realm.objects(Channel.self).filter("category = nil").sorted(byKeyPath: "url"))
            
        } else {
            return Array(realm.objects(Channel.self).filter("category.name = %@", category!.name).sorted(byKeyPath: "url"))
        }
    }
    
    func getSavedArticles() -> [Article] {
        let savedArticles = Array(realm.objects(Article.self))
        
        return savedArticles.reversed()
    }
    
    func saveToReadingRoom(article: Article) {
        if Array(realm.objects(Article.self).filter("guid = \"\(article.guid)\"")).isEmpty {
            try! realm.write {
                realm.add(article)
            }
        }
    }
    
    func removeFromReadingRoom(article: Article) {
        try! realm.write {
            realm.delete(article)
        }
    }
    
    
    func addCategory(_ category: Category) -> Bool {
        if !(realm.objects(Category.self).filter("name = \"\(category.name)\"")).isEmpty {
            return false
        }
        
        try! realm.write {
            realm.add(category)
        }
        
        return true
    }
    
    func assignCategoryToChannels(_ category: Category, _ channels: [Channel]) {
        try! realm.write {
            for ch in channels {
                ch.category = category
            }
        }
    }
    
    func modifyCategory(with channel: Channel, _ category: Category) {
        try! realm.write {
            category.channels.append(channel)
        }
    }
    
    func handleCategoryRenaming(_ category: Category, _ newName: String) throws {
        let trimmedName = newName.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if trimmedName.count == 0 || trimmedName == "nil" {
            throw Utilities.NewsFeederErrors.invalidCategoryName
        }
        
        if !(realm.objects(Category.self).filter("name = %@", newName).isEmpty) {
            throw Utilities.NewsFeederErrors.invalidCategoryName
        } else {
            try! realm.write {
                category.name = newName
                for ch in category.channels {
                    ch.category = category
                }
            }
        }
    }
    
    func modifyChannel(with category: Category? = nil, _ channel: Channel) {
        try! realm.write {
            if channel.category != nil {
                let index = channel.category?.channels.index(of: channel)
                channel.category?.channels.remove(at: index!)
            }
            
            channel.category = category == nil ? nil : category
            
            if category != nil {
                category?.channels.append(channel)
            }
        }
    }
    
    func removeCategory(_ category: Category, withChannels: Bool? = false) {
        if withChannels! {
            for ch in category.channels {
                self.removeChannel(ch, permanent: true)
            }
            
        } else {
            try! realm.write {
                for ch in category.channels {
                    ch.category = nil
                }
            }
        }
        
        try! realm.write {
            realm.delete(category)
        }
    }
    
    func getCategories() -> [Category] {
        let categories = realm.objects(Category.self)
        
        return Array(categories)
    }
    
    func getFeed(_ channels: [Channel], completion: @escaping ([Article]?, Error?) -> ()) {
        let url = URL(string:"http://54.37.139.78:80/get-articles")!
        var json: [[String:String]] = []
        if channels.isEmpty {
            completion(nil, Utilities.NewsFeederErrors.noChannelsProvided)
        }
        
        for ch in channels {
            json.append(["url":ch.url])
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.httpBody = try? JSONSerialization.data(withJSONObject: json)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = DataManager.defaultURLSession()
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            if error != nil {
                NSLog("Server error.")
                completion(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                NSLog("Response error.")
                completion(nil, Utilities.NewsFeederErrors.invalidResponse)
                return
            }
            
            guard let mime = response!.mimeType, mime == "application/json", let data = data else {
                NSLog("Invalid reponse data.")
                completion(nil, Utilities.NewsFeederErrors.invalidResponse)
                return
            }
            
            self.handleReceivedData(from: String(data: data, encoding: self.encoding)!)
            completion(self.feed, nil)
        })
        
        task.resume()
    }
    
    private func handleReceivedData(from dataString: String) {
        let regex = try! NSRegularExpression(pattern: "\\{\\s*\"title\":[^\\}]*\\}")
        let results = regex.matches(in: dataString, range: NSRange(dataString.startIndex..., in: dataString))
        let articles = results.map {
            String(dataString[Range($0.range, in: dataString)!])
        }
        
        var feed: [[String:String]] = []
        
        for json in articles {
            let articleData = json.data(using: self.encoding)
            
            if let article = try? JSONSerialization.jsonObject(with: articleData!, options: .mutableContainers) as? [String: String] {
                feed.append(article!)
            }
        }
        
        self.feed = Article.parseArticles(from:feed).reversed()
    }
    
    static func defaultURLSession(username : String = "", password:String = "") -> URLSession {
        let config = URLSessionConfiguration.default
        let userPasswordString = "\(username):\(password)"
        let userPasswordData = userPasswordString.data(using: String.Encoding.utf8)
        let base64EncodedCredential = userPasswordData!.base64EncodedData()
        let authString = "Basic \(base64EncodedCredential)"
        config.httpAdditionalHeaders = ["Authorization" : authString]
        
        return URLSession(configuration: config)
    }
}
