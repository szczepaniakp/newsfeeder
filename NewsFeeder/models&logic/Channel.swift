import Foundation
import RealmSwift
import Realm

class Channel: Object {
    
    @objc dynamic var url: String = ""
    @objc dynamic var category: Category? = nil
    
    convenience init(_ url: String, _ category: Category? = nil) {
        self.init()
        self.url = url
        self.category = category
    }
    
    convenience init(_ url: String) {
        self.init()
        self.url = url
    }
}
