import Foundation
import RealmSwift

class Article: Object {
    
    @objc dynamic var title: String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var link: String = ""
    @objc dynamic var guid: String = ""
    @objc dynamic var pubDate: String = ""
    
    convenience init(title: String, description: String, link: String, guid: String, pubDate: String) {
        self.init()
        
        self.title = title
        self.desc = description
        self.link = link
        self.guid = guid 
        self.pubDate = pubDate
    }
    
    static func parseArticles(from feed:[[String: String]]) -> [Article] {
        var results: [Article] = []
        for a in feed {
            let newArticle = Article(title: a["title"]!, description: a["description"]!, link: a["link"]!, guid: a["guid"]!, pubDate: a["pubDate"]!)
            results.append(newArticle)
        }
        
        return results
    }
}
