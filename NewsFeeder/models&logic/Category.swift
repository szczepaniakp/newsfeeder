import Foundation
import RealmSwift
import Realm

class Category: Object {
    
    @objc dynamic var name: String = ""
    var channels = RealmSwift.List<Channel>()
    
    convenience init(name: String, channels: [Channel]) {
        self.init()
        
        self.name = name
        
        for ch in channels {
            self.channels.append(ch)
        }
    }
}
