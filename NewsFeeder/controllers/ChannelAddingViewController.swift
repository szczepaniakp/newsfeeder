import UIKit

class ChannelAddingViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    var categories: [Category] = [Category(name: "-", channels: [])]
    var selectedCategory: Int = 0
    
    @IBOutlet weak var urlText: UITextField!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var addButton: UIButton!
    
    @IBAction func addButtonClicked(_ sender: Any) {
        let manager = DataManager()
        let chosenCategory = self.selectedCategory == 0 ? nil : self.categories[self.selectedCategory]
        
        if let url = self.urlText?.text {
            if url.count == 0 {
                Utilities.showAlert("No URL provided.", completion: { alert in
                    self.present(alert, animated: true, completion: nil)}
                )
                
                NSLog("No URL provided.")
                return
            }
            
            manager.addChannel(from: url, completion: {
                if $0 != nil {
                    if $0 as? Bool ?? false {
                        NSLog("Channel exists in database.")
                        Utilities.showAlert("Channel exists in database.", completion: { alert in
                            self.present(alert, animated: true, completion: nil)}
                        )
                    } else {
                        Utilities.showAlert("Invalid URL.", completion: { alert in
                            self.present(alert, animated: true, completion: nil)}
                        )
                        
                        NSLog("Invalid URL.")
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        manager.addChannelToLocalDB(with: url, to: chosenCategory)
                    }
                    
                    Utilities.showAlert("New channel URL added.", completion: { alert in
                        self.present(alert, animated: true, completion: nil)}
                    )
                    
                    NSLog("New channel URL added.")
                }
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryPicker.delegate = self
        self.categoryPicker.dataSource = self
        
        let manager = DataManager()
        self.categories.append(contentsOf: manager.getCategories())
        self.categoryPicker.selectRow(self.selectedCategory, inComponent: 0, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.categories = [Category(name: "-", channels: [])]
        
        let manager = DataManager()
        self.categories.append(contentsOf: manager.getCategories())
        
        self.categoryPicker.reloadAllComponents()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.categories[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selectedCategory = row
    }
}
