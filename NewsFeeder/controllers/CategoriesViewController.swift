import UIKit

class CategoryViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
}

class CategoriesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var viewOfCategories: UICollectionView!
    
    var categories: [Category] = [Category(name: "ALL", channels: [])]
    let cellReuseIdentifier = "categoryViewCell"
    let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    let itemsPerRow: CGFloat = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        longPressRecognizer.minimumPressDuration = 0.7
        longPressRecognizer.delegate = self
        longPressRecognizer.delaysTouchesBegan = true
        self.viewOfCategories.addGestureRecognizer(longPressRecognizer)
        
        let manager = DataManager()
        self.categories.append(contentsOf: manager.getCategories())
    }
    
    func loadData() {
        self.categories = [Category(name: "ALL", channels: [])]
        
        let manager = DataManager()
        self.categories.append(contentsOf: manager.getCategories())
        self.viewOfCategories.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadData()
    }
    
    @objc func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state != .began {
            return
        }
        
        let location = gesture.location(in: self.viewOfCategories)
        
        if let index = self.viewOfCategories.indexPathForItem(at: location) {
            if index.item == 0 {
                return
            }
            
            NSLog("Category longpressed.")
            let selectedCategory = self.viewOfCategories.cellForItem(at: index)
            self.showActionSheet(for: selectedCategory as! CategoryViewCell, at: index.item)
        }
    }
    
    private func showActionSheet(for categoryItem: CategoryViewCell, at index: Int) {
        let category = self.categories[index]
        let actionSheet = UIAlertController(title: category.name, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(self.prepareRenamingAction(for: category))
        
        actionSheet.addAction(UIAlertAction(title: "Edit", style: .default, handler: { (_) in
            let viewController: ChannelsTableViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "channelsViewController") as! ChannelsTableViewController
            viewController.category = category
            self.navigationController?.pushViewController(viewController, animated: true)
        }))
        
        actionSheet.addAction(self.prepareDeletingAction(for: category))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true)
    }
    
    private func prepareDeletingAction(for category: Category) -> UIAlertAction {
        let action = UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
            let manager = DataManager()
            let deletingPopOver = UIAlertController(title: nil, message: category.name, preferredStyle: .alert)
            deletingPopOver.addAction(UIAlertAction(title: "Delete only category", style: .destructive, handler: {
                (_) in manager.removeCategory(category)
                self.loadData()
            }))
            
            deletingPopOver.addAction(UIAlertAction(title: "Delete category with channels", style: .destructive, handler: { (_) in manager.removeCategory(category, withChannels: true)
                self.loadData()
            }))
            
            deletingPopOver.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(deletingPopOver, animated: true)
        })
        
        return action
    }
    
    private func prepareRenamingAction(for category: Category) -> UIAlertAction {
        let action = UIAlertAction(title: "Rename", style: .default, handler: { (_) in
            let renamingPopOver = UIAlertController(title: nil, message: "", preferredStyle: .alert)
            renamingPopOver.addTextField(configurationHandler: { categoryName in
                categoryName.text = category.name
            })
            
            let renamingAction = UIAlertAction(title: "Rename", style: .default, handler: { (_) in
                if let newName = renamingPopOver.textFields?.first!.text {
                    do {
                        let manager = DataManager()
                        try manager.handleCategoryRenaming(category, newName)
                        self.loadData()
                    } catch {
                        renamingPopOver.view.shake((renamingPopOver.textFields?.first)!)
                        Utilities.showAlert("Invalid new name.", completion: { alert in
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                }
            })
            
            renamingPopOver.addAction(renamingAction)
            renamingPopOver.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(renamingPopOver, animated: true)
        })
        
        return action
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.viewOfCategories.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! CategoryViewCell
        
        cell.nameLabel?.text = self.categories[indexPath.row].name
        cell.layer.backgroundColor = UIColor.lightGray.cgColor
        let sizeOfCell = self.viewOfCategories.layoutAttributesForItem(at: indexPath)!.size as CGSize
        cell.layer.cornerRadius = sizeOfCell.width / 2
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        
        if index == 0 {
            let channelsView: ChannelsTableViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "channelsViewController") as! ChannelsTableViewController
            channelsView.category = self.categories[index]
            channelsView.isAll = true
            self.navigationController?.pushViewController(channelsView, animated: true)
            
        } else {
            let feedView: FeedViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "feedController") as! FeedViewController
            feedView.selectedChannels = Array(categories[index].channels)
            self.navigationController?.pushViewController(feedView, animated: true)
        }
    }
}
