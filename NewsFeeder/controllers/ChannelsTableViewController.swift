import Foundation
import UIKit

class ChannelsTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let cellReuseIdentifier = "channelTableViewCell"
    var listOfChannels: [Channel] = []
    var isAll: Bool = false
    var category: Category? = nil
    var pickerCategories: [Category]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let manager = DataManager()
        self.listOfChannels = self.isAll ? manager.getChannels(true) : manager.getChannels(from: category)
        self.navigationController?.navigationItem.title = self.category?.name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let manager = DataManager()
        self.listOfChannels = self.isAll ? manager.getChannels(true) : manager.getChannels(from: category)
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        cell.textLabel?.text = self.listOfChannels[indexPath.row].url
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listOfChannels.count
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let manager = DataManager()
        
        let delAction = UITableViewRowAction(style: .normal, title: "DEL") { (index, action) in
            if self.isAll {
                manager.removeChannel(self.listOfChannels[indexPath.row], permanent: true)
                self.listOfChannels = manager.getChannels(self.isAll)
                NSLog("Channel deleted.")
                
            } else {
                manager.removeChannel(self.listOfChannels[indexPath.row])
                self.listOfChannels = manager.getChannels(from: self.category)
                NSLog("Channel removed from category.")
            }
            
            self.tableView.reloadData()
        }
        
        delAction.backgroundColor = .red
        self.pickerCategories = manager.getCategories()
        
        if self.isAll && self.listOfChannels[indexPath.row].category == nil && self.pickerCategories?.count != 0 {
            let addAction = UITableViewRowAction(style: .normal, title: "ADD") { (action, index) in
                self.handleChannelAssigning(self.listOfChannels[indexPath.row])
            }
            
            addAction.backgroundColor = .lightGray
            return [delAction, addAction]
        }
        
        return [delAction]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    private func handleChannelAssigning(_ channel: Channel) {
        let viewController = UIViewController()
        viewController.preferredContentSize = CGSize(width: 250,height: 150)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: 250, height: 150))
        pickerView.delegate = self
        pickerView.dataSource = self
        viewController.view.addSubview(pickerView)
        
        let categoryPickerAlert = UIAlertController(title: "Choose category", message: "\(channel.url)", preferredStyle: UIAlertController.Style.alert)
        categoryPickerAlert.setValue(viewController, forKey: "contentViewController")
        
        categoryPickerAlert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (_) in
            let pickedCategory = self.pickerCategories![pickerView.selectedRow(inComponent: 0)]
            let manager = DataManager()
            manager.assignCategoryToChannels(pickedCategory, [channel])
            manager.modifyCategory(with: channel, pickedCategory)
            NSLog("Channel assigned to category.")
            self.listOfChannels = manager.getChannels(true)
            self.tableView.reloadData()
        }))
        
        categoryPickerAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(categoryPickerAlert, animated: true)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerCategories![row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let manager = DataManager()
        return manager.getCategories().count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let addChannelView = segue.destination as? ChannelAddingViewController {
            var index: Int = 0
            
            if isAll {
                addChannelView.selectedCategory = index
                
            } else {
                let manager = DataManager()
                let categories = manager.getCategories()
                
                for c in categories {
                    if c.name == self.category?.name {
                        index = (categories.index(of: c))!
                        break
                    }
                }
                
                addChannelView.selectedCategory = index + 1
            }
        }
    }
}
