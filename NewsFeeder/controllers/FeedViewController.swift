import UIKit

class HeaderTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textLabel?.lineBreakMode = .byWordWrapping
        self.textLabel?.numberOfLines = 0
        self.textLabel?.textAlignment = .justified
        self.contentMode = .scaleToFill
        
        sizeToFit()
        layoutIfNeeded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarControllerDelegate {
    
    @IBOutlet weak var viewOfHeaders: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    var tabIndex: Int = 0
    var listOfArticles: [Article] = []
    let cellReuseIdentifier: String = "HeaderTableViewCell"
    var selectedChannels: [Channel] = []
    var isTabSelected: Bool = true
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewOfHeaders.delegate = self
        viewOfHeaders.dataSource = self
        self.tabBarController?.delegate = self
        
        self.viewOfHeaders.estimatedRowHeight = 90
        self.viewOfHeaders.rowHeight = UITableView.automaticDimension
        self.viewOfHeaders.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
        //self.viewOfHeaders.addSubview(self.refreshControl)
        
        self.tabIndex = self.tabBarController!.selectedIndex
        
        if self.tabIndex == 0 {
            let manager = DataManager()
            self.selectedChannels = manager.getChannels(true)
        }
        
        self.loadData()
    }
    
    func loadData() {
        let manager = DataManager()
        
        switch self.tabIndex {
        case 0, 1:
            self.refreshControl.beginRefreshing()
            manager.getFeed(selectedChannels,completion: { feed, error in
                guard let feed = feed, error == nil else {
                    let NFError = error as! Utilities.NewsFeederErrors
                    
                    if  NFError == Utilities.NewsFeederErrors.invalidResponse {
                        Utilities.showAlert("Server error.", completion: { alert in
                            NSLog("Server error.")
                            self.present(alert, animated: true)
                        })
                    } else if NFError == Utilities.NewsFeederErrors.noChannelsProvided {
                        Utilities.showAlert("No channels.", completion: { alert in
                            NSLog("No channels.")
                            self.present(alert, animated: true)
                        })
                    } else {
                        Utilities.showAlert("Unknown error.", completion: { alert in
                            NSLog("Unknown error.")
                            self.present(alert, animated: true)
                        })
                    }
                    
                    self.refreshControl.endRefreshing()
                    return
                }
                
                DispatchQueue.main.async(execute: {
                    self.listOfArticles = feed
                    self.refreshControl.endRefreshing()
                    self.viewOfHeaders.reloadData()
                })
            })
        case 2:
            self.listOfArticles = manager.getSavedArticles()
            self.refreshControl.endRefreshing()
            self.viewOfHeaders.reloadData()
            
        default:
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let manager = DataManager()
        
        switch self.tabIndex {
        case 0:
            let channels = manager.getChannels(true)
            if self.selectedChannels.isEmpty {
                self.selectedChannels = channels
                self.loadData()
            } else {
                self.selectedChannels = channels
            }
            
        case 2:
            self.loadData()
        default:
            break
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.isTabSelected = (self.tabIndex != 1) ? true : false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isTabSelected = false
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.viewOfHeaders.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! HeaderTableViewCell
        cell.textLabel?.text = self.listOfArticles[indexPath.row].title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listOfArticles.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("Article in \(indexPath.row). row was selected.")
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let manager = DataManager()
        
        switch self.tabIndex {
        case 0:
            let addToRRAction = UITableViewRowAction(style: .normal, title: "SAVE") { action, index in
                let selectedArticle = self.listOfArticles[index.row]
                manager.saveToReadingRoom(article: selectedArticle)
                NSLog("Article was added to Reading Room.")
            }
            
            addToRRAction.backgroundColor = .lightGray
            return [addToRRAction]
            
        case 2:
            let deleteAction = UITableViewRowAction(style: .destructive, title: "DEL") { action, index in
                manager.removeFromReadingRoom(article: self.listOfArticles[index.row])
                self.listOfArticles.remove(at: index.row)
                self.viewOfHeaders.reloadData()
                NSLog("Article was deleted.")
            }
            
            deleteAction.backgroundColor = .red
            return [deleteAction]
            
        default:
            return []
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let view = tabBarController.selectedViewController?.children[0]
        if view is FeedViewController {
            let selectedView = view as! FeedViewController
            if selectedView.isTabSelected {
                self.viewOfHeaders.setContentOffset(CGPoint.zero, animated: true)
            }
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        self.loadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let articleView = segue.destination as? ArticleViewController {
            
            guard let index = self.viewOfHeaders.indexPathForSelectedRow?.row as Int? else {
                NSLog("Row error.")
                return
            }
            
            articleView.article = self.listOfArticles[index]
        }
    }
}


