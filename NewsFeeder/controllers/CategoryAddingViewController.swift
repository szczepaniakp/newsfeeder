import UIKit

class ChannelTableViewCell: UITableViewCell {
    
    var datasource: Channel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutMargins = UIEdgeInsets.zero
        self.separatorInset = UIEdgeInsets.zero
    }
}

class CategoryAddingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var listOfAvailableChannels: [Channel] = []
    var selectedChannels: [Channel] = []
    
    @IBOutlet weak var categoryName: UITextField!
    @IBOutlet weak var viewOfAvailableChannels: UITableView!
    
    @IBAction func addCategoryButtonClicked(_ sender: Any) {
        if var name = (self.categoryName?.text) {
            name = name.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if name.count == 0 {
                Utilities.showAlert("No name provided.", completion: { alert in
                    self.present(alert, animated: true, completion: nil)
                })
                
                NSLog("No name provided.")
                return
            }
            
            if name == "nil" {
                Utilities.showAlert("Forbidden name used.", completion: { alert in
                    self.present(alert, animated: true, completion: nil)
                })
                
                NSLog("Forbidden name used.")
                return
            }
            
            let manager = DataManager()
            let category = Category(name: name, channels: selectedChannels)
            
            if manager.addCategory(category) {
                NSLog("Category added.")
                manager.assignCategoryToChannels(category, selectedChannels)
                self.listOfAvailableChannels = manager.getChannels()
                self.categoryName?.text = ""
                self.viewOfAvailableChannels.reloadData()
                
                Utilities.showAlert("Category added.", controller: self, completion: { alert in
                    self.present(alert, animated: true, completion: nil)
                })
                
            } else {
                Utilities.showAlert("Category exists in database.", completion: { alert in
                    self.present(alert, animated: true, completion: nil)
                })
                
                NSLog("Category exists in database.")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewOfAvailableChannels.delegate = self
        self.viewOfAvailableChannels.dataSource = self
        self.viewOfAvailableChannels.allowsMultipleSelectionDuringEditing = true
        self.viewOfAvailableChannels.setEditing(true, animated: false)
        
        let manager = DataManager()
        self.listOfAvailableChannels = manager.getChannels()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let manager = DataManager()
        self.listOfAvailableChannels = manager.getChannels()
        self.viewOfAvailableChannels.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listOfAvailableChannels.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = viewOfAvailableChannels.cellForRow(at: indexPath as IndexPath) as! ChannelTableViewCell
        self.selectedChannels.append(cell.datasource!)
        
        self.viewOfAvailableChannels.cellForRow(at: indexPath as IndexPath)?.accessoryType = UITableViewCell.AccessoryType.checkmark
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.viewOfAvailableChannels.dequeueReusableCell(withIdentifier: "channelTableViewCell", for: indexPath) as! ChannelTableViewCell
        cell.textLabel?.text = self.listOfAvailableChannels[indexPath.row].url
        cell.datasource = self.listOfAvailableChannels[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = viewOfAvailableChannels.cellForRow(at: indexPath as IndexPath) as! ChannelTableViewCell
        let index = self.selectedChannels.index(of: cell.datasource!)!
        self.selectedChannels.remove(at: index)
        
        self.viewOfAvailableChannels.cellForRow(at: indexPath as IndexPath)?.accessoryType = UITableViewCell.AccessoryType.none
    }
}
