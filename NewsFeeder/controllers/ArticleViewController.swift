import UIKit
import SafariServices

class ArticleViewController: UIViewController, SFSafariViewControllerDelegate {
    
    var article: Article?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionField: UILabel!
    @IBOutlet weak var pubDateLabel: UILabel!
    
    @IBAction func openSafariView(_ sender: Any) {
        let safariView = SFSafariViewController(url: URL(string: self.article!.link)!)
        self.present(safariView, animated: true, completion: nil)
        safariView.delegate = self
        NSLog("Atricle opened.")
    }
    
    @IBAction func shareArticle(_ sender: Any) {
        let url = NSURL(fileURLWithPath: self.article?.link ?? "")
        
        let activityViewController = UIActivityViewController(activityItems: [self.article?.link ?? "", url], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        present(activityViewController, animated: true)
        NSLog("Atricle sharing.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = article?.title
        
        self.descriptionField.attributedText = (article?.desc)?.htmlToAttributedString
        self.pubDateLabel.text = article?.pubDate
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
        NSLog("Atricle closed.")
    }
}
